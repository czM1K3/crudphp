<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Crud PHP
This is second school project on [DELTA - Střední škola informatiky a ekonomie, s.r.o.](https://www.delta-skola.cz/) in a 3rd year of study.

## Requirements
- LAMP server (check [Laravel documentation](https://laravel.com/docs/8.x/installation))
- Composer

## Installation
1. Clone this project `git clone git@gitlab.com:czM1K3/crudphp.git`
2. Open terminal in this folder
3. Type: `composer install`
4. Add a folder "Public" to Apache
5. Restart Apache
6. Make copy of .env.example and name it .env
7. Open .env and fill in your MySQL/MariaDB credentials
8. Type: `php artisan migrate`
9. You are all set
