<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Room;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function  __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sort = filter_input(INPUT_GET, 'sort', FILTER_VALIDATE_INT, array("options" => array("default" => 0, "min_range" => 0, "max_range" => 7)));
        $users = User::all();
        if ($sort === 0) $users = $users->sortBy("surname");
        elseif ($sort === 1) $users = $users->sortByDesc("surname");
        elseif ($sort === 2) $users = $users->sortBy("job");
        elseif ($sort === 3) $users = $users->sortByDesc("job");
        elseif ($sort === 4) $users = $users->sortBy("phone");
        elseif ($sort === 5) $users = $users->sortByDesc("phone");
        elseif ($sort === 6) $users = $users->sortBy("room");
        elseif ($sort === 7) $users = $users->sortByDesc("room");

        return view("users.index")->with("users", $users)->with("sort", $sort)->with("title", "Seznam zaměstnanců");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->admin)  return redirect(url("/user"))->with("error", "Nemáš právo to udělat");
        $rooms = array();
        foreach (Room::all() as $room) {
            $rooms[$room->id] = $room->name;
        }
        return view("users.create")->with("rooms", $rooms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Auth::user()->admin)  return redirect(url("/user"))->with("error", "Nemáš právo to udělat");
        $this->validate($request, [
            "name" => "required|string",
            "surname" => "required|string",
            "email" => "required|email:rfc,dns",
            "password" => "required|min:8|confirmed",
            "job" => "required|string",
            "wage" => "numeric|required",
            "number" => "numeric|required",
            "admin" => "boolean",
        ]);

        $user = new User;
        $user->name = $request->input("name");
        $user->surname = $request->input("surname");
        $user->email = $request->input("email");
        $user->password = Hash::make($request->input("password"));
        $user->job = $request->input("job");
        $user->wage = $request->input("wage");
        $user->room = $request->input("number");
        $user->admin = $request->input("admin") ?? "0";
        $user->save();

        return redirect(url("/user"))->with("success", "Zaměstnanec vytvořen");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if(is_null($user)) return redirect(url("/user"))->with("error", "Zaměstnanec neexistuje")->with("title","Sezname zaměstnanců");
        return view("users.show")->with("user", $user)->with("title", "Karta osoby: " . $user->surname . " " . $user->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->admin)  return redirect(url("/user/".$id))->with("error", "Nemáš právo to udělat");
        $user = User::find($id);
        if(is_null($user)) return redirect(url("/user"))->with("error", "Zaměstnanec neexistuje")->with("title","Sezname zaměstnanců");
        $rooms = array();
        foreach (Room::all() as $room) {
            $rooms[$room->id] = $room->name;
        }
        return view("users.edit")->with("user", $user)->with("rooms", $rooms);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!Auth::user()->admin)  return redirect(url("/user/".$id))->with("error", "Nemáš právo to udělat");
        $this->validate($request, [
            "name" => "required|string",
            "surname" => "required|string",
            "email" => "required|email:rfc,dns",
            "job" => "required|string",
            "wage" => "numeric|required",
            "number" => "numeric|required",
            "admin" => "boolean",
        ]);

        $user = User::find($id);
        $user->name = $request->input("name");
        $user->surname = $request->input("surname");
        $user->email = $request->input("email");
        $user->job = $request->input("job");
        $user->wage = $request->input("wage");
        $user->room = $request->input("number");
        $user->admin = $request->input("admin") ?? "0";
        $user->save();
        return redirect(url("/user/".$id))->with("success", "Zaměstnanec upraven");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->admin)  return redirect(url("/user/".$id))->with("error", "Nemáš právo to udělat");
        $user = User::find($id);
        if($user->id === Auth::user()->id) return redirect(url("/user/".$id))->with("error", "Nemůžeš vymazat sám sebe");
        if(count($user->keyRel) > 0) return redirect(url("/user/".$id))->with("error", "Zaměstnanec má nějaké klíče");
        $user->delete();
        return redirect(url("/user"))->with("success", "Zaměstnanec smazán");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        return view("users.changepassword");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function passwordUpdate(Request $request)
    {
        $this->validate($request, [
            "oldPassword" => "required|string",
            "newPassword" => "required|min:8|confirmed",
        ]);
        $user = Auth::user();
        if(!Hash::check($request->input("oldPassword"),$user->password)) return redirect(url("/changepassword"))->with("error", "Špatné staré heslo");
        $user->password = Hash::make($request->input("newPassword"));
        $user->save();
        return redirect(url("/user/".$user->id))->with("success", "Zaměstnanec upraven");
    }
}
