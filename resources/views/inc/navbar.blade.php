<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link {{is_null(Route::current()->getName()) ? "active":""}}" aria-current="page" href="{{url("/")}}">Domů</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{\Illuminate\Support\Str::contains(Route::current()->getName(), "room") ? "active":""}}" aria-current="page" href="{{url("/room")}}">Místnosti</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{\Illuminate\Support\Str::contains(Route::current()->getName(), "user") ? "active":""}}" href="{{url("/user")}}">Zaměstnanci</a>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="btn btn-primary" href="{{url("/login")}}">{{ __('Přihlásit se') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{url("/user/".Auth::user()->id)}}" role="button">
                            {{ Auth::user()->surname }} {{Auth::user()->name}}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-primary m-1" href="{{url("/changepassword")}}" role="button">
                            Změnit heslo
                        </a>
                    </li>
                    <li class="nav-item">


                        {!! Form::open(['action' => "App\Http\Controllers\Auth\LoginController@logout", "method" => "POST"]) !!}
                        {{Form::submit("Odhlásit se", ["class" => "btn btn-primary m-1"])}}
                        {!! Form::close() !!}
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
