@extends("layouts.app")

@section("content")
    <h1>Vytvořit klíč</h1>

    {!! Form::open(['action' => "App\Http\Controllers\KeyController@store", "method" => "POST"]) !!}
    <div class="form-check">
        {{Form::label("Zaměstnanec")}}
        {{Form::select('user', $users, $default)}}
    </div>
    <div class="form-check">
        {{Form::label("Místnost")}}
        {{Form::select('room', $rooms)}}
    </div>
    {{Form::submit("Vytvořit", ["class" => "btn btn-success m-1"])}}
    {!! Form::close() !!}
    <a href="{{url("/user")}}" class="btn btn-primary m-1">Zpět</a>
@endsection
