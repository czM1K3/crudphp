@extends("layouts.app")

@section("content")
<h1>Prohlížeč imaginárni firmy</h1>
<div class="d-flex justify-content-center">
    <a class="btn btn-primary m-1" href="{{url("/room")}}">Místnosti</a>
    <a class="btn btn-primary m-1" href="{{url("/user")}}">Zaměstnanci</a>
</div>
    <p>Tento projekt je Open-source na <a href="https://gitlab.com/czM1K3/crudphp">GitLabu</a>, kde je včetně dokumentace.</p>
@endsection
