@extends("layouts.app")

@section("content")
    <h1>Vytvořit místnost</h1>

    {!! Form::open(['action' => "App\Http\Controllers\RoomController@store", "method" => "POST"]) !!}
    <div class="form-check">
        {{Form::label("Jméno")}}
        {{Form::text("name", "", ["class" => "form-control", "placeholder" => "Jméno"])}}
    </div>
    <div class="form-check">
        {{Form::label("Číslo místnosti")}}
        {{Form::number("number", "", ["class" => "form-control", "placeholder" => "Číslo místnosti"])}}
    </div>
    <div class="form-check">
        {{Form::label("Telefon")}}
        {{Form::number("phone", "", ["class" => "form-control", "placeholder" => "Telefon"])}}
    </div>
    {{Form::submit("Vytvořit", ["class" => "btn btn-success m-1"])}}
    {!! Form::close() !!}
    <a href="{{url("/room")}}" class="btn btn-primary m-1">Zpět</a>
@endsection
