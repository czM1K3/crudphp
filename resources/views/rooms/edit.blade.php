@extends("layouts.app")

@section("content")
        <h1>Upravit místnost</h1>

        {!! Form::open(['action' => ["App\Http\Controllers\RoomController@update", $room->id], "method" => "POST"]) !!}
        <div class="form-check">
            {{Form::label("Jméno")}}
            {{Form::text("name", $room->name, ["class" => "form-control", "placeholder" => "Jméno"])}}
        </div>
        <div class="form-check">
            {{Form::label("Číslo místnosti")}}
            {{Form::number("number", $room->no, ["class" => "form-control", "placeholder" => "Číslo místnosti"])}}
        </div>
        <div class="form-check">
            {{Form::label("Telefon")}}
            {{Form::number("phone", $room->phone, ["class" => "form-control", "placeholder" => "Telefon"])}}
        </div>
        {{Form::hidden("_method", "PUT")}}
        {{Form::submit("Uložit", ["class" => "btn btn-success m-1"])}}
        {!! Form::close() !!}
        <a href="{{url("/room/".$room->id)}}" class="btn btn-primary m-1">Zpět</a>
@endsection
