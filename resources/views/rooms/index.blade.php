@extends("layouts.app")

@section("content")
    <h1>Místnosti</h1>
    @if(count($rooms) > 0)
        <table class="table table-striped">
            <thead class="highlight">
            <tr>
                <th>Jméno
                    <a href="?sort=0" @if($sort === 0) class="text-danger" @endif>⬇</a>
                    <a href="?sort=1" @if($sort === 1) class="text-danger" @endif >⬆</a>
                </th>
                <th>Číslo místnosti
                    <a href="?sort=2" @if($sort === 2) class="text-danger" @endif>⬇</a>
                    <a href="?sort=3" @if($sort === 3) class="text-danger" @endif>⬆</a>
                </th>
                <th>Telefon
                    <a href="?sort=4" @if($sort === 4) class="text-danger" @endif>⬇</a>
                    <a href="?sort=5" @if($sort === 5) class="text-danger" @endif>⬆</a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($rooms as $room)
                <tr>
                    <td><a href="{{url("/room/".$room->id)}}">{{$room->name}}</a></td>
                    <td>{{$room->no}}</td>
                    <td>{{$room->phone}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>Žádná místnost</p>
    @endif
    @if(Auth::user()->admin)
    <a href="{{url("/room/create")}}" class="btn btn-success m-1">Vytvořit místnost</a>
    @endif
@endsection
