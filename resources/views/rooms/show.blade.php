@extends("layouts.app")

@section("content")
    <h1>Místnost {{$room->no}}</h1>
    <dl>
        <dt>Číslo místnosti</dt>
        <dd>{{$room->no}}</dd>

        <dt>Název místnosti</dt>
        <dd>{{$room->name}}</dd>

        <dt>Telefon</dt>
        @if($room->phone != null)
            <dd>{{$room->phone}}</dd>
        @else
            <dd>Není</dd>
        @endif

        <dt>Lidé</dt>
        <dd>
            @if(count($room->userRel) > 0)
                @foreach($room->userRel as $user)
                    <a href="{{url("/user/".$user->id)}}">{{$user->surname}} {{$user->name}}</a><br>
                @endforeach
            @else
            <p>Nikdo</p>
            @endif
        </dd>

        <dt>Průměrný plat</dt>
        <dd>
            {{round($average)}} Kč
        </dd>

        <dt>Klíče</dt>
        <dd>
            @if(count($room->keyRel) > 0)
                @foreach($room->keyRel as $key)
                    <a href="{{url("/user/".$key->userRel->id)}}">{{$key->userRel->surname}} {{$key->userRel->name}}</a><br>
                @endforeach
            @else
                <p>Nikdo</p>
            @endif
        </dd>
    </dl><br>
    @if(Auth::user()->admin)
    <a href="{{url("/room/".$room->id."/edit")}}" class="btn btn-primary m-1">Upravit</a>
    {!! Form::open(["action" => ["App\Http\Controllers\RoomController@destroy", $room->id], "method" => "POST", "class" => "pull-right"]) !!}
        {{Form::hidden("_method", "DELETE")}}
        {{Form::submit("Odstranit", ["class" => "btn btn-danger m-1"])}}
    {!! Form::close() !!}
    @endif
    <a href="{{url("/room")}}" class="btn btn-primary m-1">Zpět</a>
@endsection
