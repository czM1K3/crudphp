@extends("layouts.app")

@section("content")
<h1>Změnit heslo</h1>

{!! Form::open(['action' => "App\Http\Controllers\UserController@passwordUpdate", "method" => "POST"]) !!}

<div class="form-check">
    {{Form::label("Staré heslo")}}
    {{Form::password("oldPassword", ["class" => "form-control", "placeholder" => "Heslo"])}}
</div>

<div class="form-check">
    {{Form::label("Nové Heslo")}}
    {{Form::password("newPassword", ["class" => "form-control", "placeholder" => "Heslo"])}}
</div>
<div class="form-check">
    {{Form::label("Znovu nové heslo")}}
    {{Form::password("newPassword_confirmation", ["class" => "form-control", "placeholder" => "Heslo"])}}
</div>
{{Form::hidden("_method", "PUT")}}
{{Form::submit("Změnit", ["class" => "btn btn-success m-1"])}}
{!! Form::close() !!}
<a href="{{url("/")}}" class="btn btn-primary m-1">Zpět</a>
@endsection
