@extends("layouts.app")

@section("content")
    <h1>Vytvořit zaměstnance</h1>

    {!! Form::open(['action' => "App\Http\Controllers\UserController@store", "method" => "POST"]) !!}
    <div class="form-check">
        {{Form::label("Jméno")}}
        {{Form::text("name", "", ["class" => "form-control", "placeholder" => "Jméno"])}}
    </div>
    <div class="form-check">
        {{Form::label("Příjmení")}}
        {{Form::text("surname", "", ["class" => "form-control", "placeholder" => "Příjmení"])}}
    </div>
    <div class="form-check">
        {{Form::label("Email")}}
        {{Form::text("email", "", ["class" => "form-control", "placeholder" => "Email"])}}
    </div>
    <div class="form-check">
        {{Form::label("Heslo")}}
        {{Form::password("password", ["class" => "form-control", "placeholder" => "Heslo"])}}
    </div>
    <div class="form-check">
        {{Form::label("Znovu heslo")}}
        {{Form::password("password_confirmation", ["class" => "form-control", "placeholder" => "Heslo"])}}
    </div>
    <div class="form-check">
        {{Form::label("Pozice")}}
        {{Form::text("job", "", ["class" => "form-control", "placeholder" => "Pozice"])}}
    </div>
    <div class="form-check">
        {{Form::label("Mzda")}}
        {{Form::number("wage", "", ["class" => "form-control", "placeholder" => "Mzda"])}}
    </div>
    <div class="form-check">
        {{Form::label("Místnost")}}
        {{Form::select('number', $rooms)}}
    </div>
    <div class="form-check">
        {{Form::label("Admin")}}
        {{Form::checkbox('admin', 1)}}
    </div>
    {{Form::submit("Vytvořit", ["class" => "btn btn-success m-1"])}}
    {!! Form::close() !!}
    <a href="{{url("/user")}}" class="btn btn-primary m-1">Zpět</a>
@endsection
