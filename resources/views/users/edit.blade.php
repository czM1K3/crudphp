@extends("layouts.app")

@section("content")
        <h1>Upravit zaměstnance</h1>

        {!! Form::open(['action' => ["App\Http\Controllers\UserController@update", $user->id], "method" => "POST"]) !!}
        <div class="form-check">
            {{Form::label("Jméno")}}
            {{Form::text("name", $user->name, ["class" => "form-control", "placeholder" => "Jméno"])}}
        </div>
        <div class="form-check">
            {{Form::label("Příjmení")}}
            {{Form::text("surname", $user->surname, ["class" => "form-control", "placeholder" => "Příjmení"])}}
        </div>
        <div class="form-check">
            {{Form::label("Email")}}
            {{Form::text("email", $user->email, ["class" => "form-control", "placeholder" => "Email"])}}
        </div>
        <div class="form-check">
            {{Form::label("Pozice")}}
            {{Form::text("job", $user->job, ["class" => "form-control", "placeholder" => "Pozice"])}}
        </div>
        <div class="form-check">
            {{Form::label("Mzda")}}
            {{Form::number("wage", $user->wage, ["class" => "form-control", "placeholder" => "Mzda"])}}
        </div>
        <div class="form-check">
            {{Form::label("Místnost")}}
            {{Form::select('number', $rooms, $user->room)}}
        </div>
        <div class="form-check">
            {{Form::label("Admin")}}
            {{Form::checkbox('admin', 1, $user->admin)}}
        </div>
        {{Form::hidden("_method", "PUT")}}
        {{Form::submit("Uložit", ["class" => "btn btn-success m-1"])}}
        {!! Form::close() !!}
        <a href="{{url("/user/".$user->id)}}" class="btn btn-primary m-1">Zpět</a>
@endsection
