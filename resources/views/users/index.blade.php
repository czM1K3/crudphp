@extends("layouts.app")

@section("content")
    <h1>Zaměstnanci</h1>
    @if(count($users) > 0)
        <table class="table table-striped">
            <thead class="highlight">
            <tr>
                <th>Jméno
                    <a href="?sort=0" @if($sort === 0) class="text-danger" @endif>⬇</a>
                    <a href="?sort=1" @if($sort === 1) class="text-danger" @endif >⬆</a>
                </th>
                <th>Pozice
                    <a href="?sort=2" @if($sort === 2) class="text-danger" @endif>⬇</a>
                    <a href="?sort=3" @if($sort === 3) class="text-danger" @endif>⬆</a>
                </th>
                <th>Telefon
                    <a href="?sort=4" @if($sort === 4) class="text-danger" @endif>⬇</a>
                    <a href="?sort=5" @if($sort === 5) class="text-danger" @endif>⬆</a>
                </th>
                <th>Místnost
                    <a href="?sort=6" @if($sort === 6) class="text-danger" @endif>⬇</a>
                    <a href="?sort=7" @if($sort === 7) class="text-danger" @endif>⬆</a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td><a href="{{url("/user/".$user->id)}}">{{$user->surname . " " . $user->name}}</a></td>
                    <td>{{$user->job}}</td>
                    <td>{{$user->roomRel->phone}}</td>
                    <td>{{$user->roomRel->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>Žádný zaměstnanec</p>
    @endif
    @if(Auth::user()->admin)
    <a href="{{url("/user/create")}}" class="btn btn-success m-1">Vytvořit zaměstnance</a>
    @endif
@endsection
