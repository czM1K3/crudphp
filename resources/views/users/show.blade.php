@extends("layouts.app")

@section("content")
    <h1>Karta osoby: {{$user->surname}} {{$user->name}}</h1>
    <dl>
        <dt>Jméno</dt>
        <dd>{{$user->name}}</dd>

        <dt>Příjmení</dt>
        <dd>{{$user->surname}}</dd>

        <dt>Pozice</dt>
        <dd>{{$user->job}}</dd>

        <dt>Mzda</dt>
        <dd>{{$user->wage}}</dd>

        <dt>Email</dt>
        <dd><a href="mailto:{{$user->email}}">{{$user->email}}</a></dd>

        <dt>Admin</dt>
        <dd>{{$user->admin ? "Ano" : "Ne"}}</dd>

        <dt>Místnost</dt>
        <dd><a href="{{url("/room/".$user->roomRel->id)}}">{{$user->roomRel->name}}</a></dd>

        <dt>Klíče</dt>
        <dd>
            @if(count($user->keyRel) > 0)
                @foreach($user->keyRel as $key)
                    <a href="{{url("/room/".$key->roomRel->id)}}">{{$key->roomRel->name}}</a>
                    @if(Auth::user()->admin)
                    {!! Form::open(["action" => ["App\Http\Controllers\KeyController@destroy", $key->id], "method" => "POST", "class" => "pull-right"]) !!}
                    {{Form::hidden("_method", "DELETE")}}
                    {{Form::submit("Odstranit", ["class" => "btn btn-danger m-1"])}}
                    {!! Form::close() !!}
                    @else
                        <br>
                    @endif
                @endforeach
            @else
                Nic<br>
            @endif
            @if(Auth::user()->admin)
                <a href="{{url("/key/create?employee=").$user->id}}" class="btn btn-success">Přidat klíč</a>
                @endif
        </dd>
    </dl><br>
    @if(Auth::user()->admin)
    <a href="{{url("/user/".$user->id."/edit")}}" class="btn btn-primary m-1">Upravit</a>
    {!! Form::open(["action" => ["App\Http\Controllers\UserController@destroy", $user->id], "method" => "POST", "class" => "pull-right"]) !!}
        {{Form::hidden("_method", "DELETE")}}
        {{Form::submit("Odstranit", ["class" => "btn btn-danger m-1"])}}
    {!! Form::close() !!}
        @endif
    <a href="{{url("/user")}}" class="btn btn-primary m-1">Zpět</a>
@endsection
